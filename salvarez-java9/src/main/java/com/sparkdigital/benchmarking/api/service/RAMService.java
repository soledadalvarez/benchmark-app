package com.sparkdigital.benchmarking.api.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Service that reads a file and counts vowels
 * 
 * @author Soledad
 *
 */
public class RAMService implements BenchmarkService {

	private final Pattern PATTERN = Pattern.compile("[aeiouAEIOU]");
	private final Path PATH = Paths.get("support/ram_test.txt");

	/**
	 * Returns a Json with the number of vowels
	 * 
	 * @throws IOException
	 */
	@Override
	public String process() throws IOException {
		String content = new String(Files.readAllBytes(PATH));
		Matcher matcher = PATTERN.matcher(content);
		return "{ \"n_vowels\": " + matcher.results().count() + " }\"";
	}

}
